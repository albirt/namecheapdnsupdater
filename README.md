# namecheapDNSUpdater  
This program dinamically updates the IP of your machine for use with namecheap DNS.  
###### This program only works on GNU/Linux Operating Systems.  
## Installation  
To install, log into your server and run  
`git clone https://gitlab.com/albirt/namecheapdnsupdater.git`  
then `cd namecheapdnsupdater` and `sudo ./install.sh`, then follow the instructions in the terminal.  
This will create a .service file and an associated .timer file which will update the IP every 5 minutes.  
Sudo privileges are required to install the program as a service and a timer which ensure constant updates of the IP address.  
## What this program does  
This program updates your IP via namecheap's [official IP update link](https://www.namecheap.com/support/knowledgebase/article.aspx/29/11/how-do-i-use-a-browser-to-dynamically-update-the-hosts-ip/), through `curl` with a script which is saved in the /opt/ directory  