#!/bin/bash

# Function to call if a command fails unrecoverably
f_quit () {
    echo "$1"
    exit 1
}

# Read host
echo "Enter your desired host (ctrl-D for default (@)): "
read host || host=@

# Read domain
echo "Enter your domain: "
read domain || f_quit "You must enter a domain!"

# Read password
echo "Enter your DNS password (NOT your account password!): "
read password || f_quit "You must enter a password!"

echo "Host: $host, domain: $domain, password: $password"

umask 077

# Create service file
touch /lib/systemd/system/namecheapDNSUpdater${host}.${domain}.service || f_quit "Couldn't create service file, did you run as sudo?"

# Write service file
echo "[Unit]
Description=Update namecheap DNS IP for ${host}.${domain}
After=multi-user.target

[Service]
ExecStart=curl 'https://dynamicdns.park-your-domain.com/update?host=${host}&domain=${domain}&password=${password}'

DynamicUser=yes

[Install]
WantedBy=multi-user.target" > /lib/systemd/system/namecheapDNSUpdater${host}.${domain}.service || f_quit "Couldn't write to service file, did you run as sudo?"

# Create timer file
touch /lib/systemd/system/namecheapDNSUpdater${host}.${domain}.timer || f_quit "Couldn't create timer file, did you run as sudo?"

# Write timer file
echo "[Unit]
Description=Run namecheapDNSUpdater for ${host}.${domain} every 5 minutes

[Timer]
OnBootSec=5min
OnUnitActiveSec=5min

[Install]
WantedBy=timers.target" > /lib/systemd/system/namecheapDNSUpdater${host}.${domain}.timer || f_quit "Couldn't write to timer file, did you run as sudo?"

# Reload systemctl daemon
echo "Reloading systemctl daemon"
systemctl daemon-reload || f_quit "Couldn't reload daemon, did you run as sudo?"

# Start service
echo "Starting service"
systemctl start namecheapDNSUpdater${host}.${domain}.service || f_quit "Couldn't start service, did you run as sudo?"

# Enable service
echo "Enabling service"
systemctl enable namecheapDNSUpdater${host}.${domain}.service || f_quit "Couldn't enable service, did you run as sudo?"

# Start timer
echo "Starting timer"
systemctl start namecheapDNSUpdater${host}.${domain}.timer || f_quit "Couldn't start timer, did you run as sudo?"

# Enable timer
echo "Enabling timer"
systemctl enable namecheapDNSUpdater${host}.${domain}.timer || f_quit "Couldn't enable timer, did you run as sudo?"
